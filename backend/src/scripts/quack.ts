import dotenv from 'dotenv';
dotenv.config();

const TWITCH_CHANNEL = process.env.TWITCH_CHANNEL;
const TWITCH_USER = process.env.TWITCH_USER;
const TWITCH_USER_TOKEN = process.env.TWITCH_USER_TOKEN;
const TWITCH_CLIENT_ID = process.env.TWITCH_CLIENT_ID;
const TWITCH_CLIENT_TOKEN = process.env.TWITCH_TOKEN;
const TWITCH_MESSAGE_TIMEOUT = parseInt(process.env.TWITCH_MESSAGE_TIMEOUT ?? '1000');

// Instancio TMI
const tmi = require('tmi.js');

// Configuración de la conexión a Twitch
const client = new tmi.Client({
    options: { debug: true },
    connection: {
        secure: true,
        reconnect: true
    },
    identity: {
        username: TWITCH_USER,
        password: TWITCH_USER_TOKEN
    },
    channels: [TWITCH_CHANNEL]
});

var iterations = 0;
var iterationsLimit = 100; // 0 = infinito
var isActive = true;

// Evento que se ejecuta cuando la conexión está lista
client.on('connected', () => {
    console.log('Conectado a Twitch');

    // Envío de mensajes cada segundo
    let interval = setInterval(() => {
        if (!isActive) return;
        client.say(TWITCH_CHANNEL, '*quack*');
        iterations++;

        // Si llega al límite de iteraciones, se para el intervalo
        if (iterationsLimit !== 0 && iterations >= iterationsLimit) {
            console.log('Llega al límite de iteraciones');

            isActive = false;
            clearInterval(interval);
        }

    }, TWITCH_MESSAGE_TIMEOUT);
});

// Evento que se ejecuta cuando se recibe un mensaje en el chat
//client.on('message', (channel, tags, message, self) => {
client.on('message', (channel, tags, message, self) => {
    //console.log(`${tags['display-name']}: ${message}`);

    // Si el mensaje es "!stop", se para el intervalo
    if (tags.username === TWITCH_USER && message === '!quackon') {
        isActive = true;
    } else if (tags.username === TWITCH_USER && message === '!quackoff') {
        isActive = false;
    }

});

// Conexión a Twitch
client.connect();
